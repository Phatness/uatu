
Pod::Spec.new do |s|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  s.name         = "Uatu"
  s.version      = "0.9.0"
  s.summary      = "Uatu: a low-friction Observable library."
  s.description  = <<-DESC
    Uatu is a small library to add Observable functionality to your iOS project with (theoretically) no interference.
    It is based on ideas from RxSwift and Observables.
                   DESC
  s.homepage     = "https://bitbucket.org/sirshannon/uatu/"

  # ―――  Spec License  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  s.license      = { :type => "MIT", :file => "LICENSE" }

  # ――― Author Metadata  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  s.author             = "sirshannon"
  s.social_media_url   = "http://twitter.com/_helpermonkey"

  # ――― Platform Specifics ――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  s.platform     = :ios, "11"

  # ――― Source Location ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  s.source = { :git => "https://bitbucket.org/sirshannon/uatu", :tag => "#{s.version}" }

  # ――― Source Code ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  s.source_files  = "Uatu", "Uatu/**/*.{h,m,swift}"

  # ――― Resources ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  s.pod_target_xcconfig = { 'SWIFT_VERSION' => '4' }
  s.swift_version = "4"

end
