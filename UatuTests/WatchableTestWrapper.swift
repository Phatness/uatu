//
//  WatchableTestWrapper.swift
//  UatuTests
//

import Foundation
@testable import Uatu

public class WatchableTestWrapper<T> {
    
    let watchable: Watchable<T>
    public var disposeBag: DisposeBag?
    
    private let pauseBetweenValues = 100
    private let useThreads: Bool
    
    private let someQueue = DispatchQueue(label: "some queue", qos: .userInitiated, attributes: [])
    private let anotherQueue = DispatchQueue(label: "another queue", qos: .userInitiated, attributes: [])
    private let otherQueue = DispatchQueue(label: "other queue", qos: .userInitiated, attributes: [])
    private let randomQueue = DispatchQueue(label: "random queue", qos: .userInitiated, attributes: [])
    private let concurrentQueue = DispatchQueue(label: "concurrent queue", qos: .userInitiated, attributes: [DispatchQueue.Attributes.concurrent])
    
    init(watchable: Watchable<T>, useThreads: Bool? = false) {
        
        self.watchable = watchable
        self.useThreads = useThreads ?? false
    }
    
    public func setValuesAndContinue(values: [T]) {
        
        nextValueAndContinue(values: values, index: 0)
    }
    
    private func nextValueAndContinue(values: [T], index: Int) {
    
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(pauseBetweenValues)) {
            
            guard self.useThreads else {
                guard index < values.count else {
                    return
                }
                self.watchable.value = values[index]
                self.nextValueAndContinue(values: values, index: index + 1)
                return
            }
            
            self.someQueue.sync {
                guard index < values.count else {
                    return
                }
                self.watchable.value = values[index]
                self.nextValueAndContinue(values: values, index: index + 1)
            }
        }
    }
    
    public func setValuesAndEnd(values: [T]) {
        
        nextValueAndEnd(values: values, index: 0)
    }
    
    private func nextValueAndEnd(values: [T], index: Int) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(pauseBetweenValues)) {
            
            guard self.useThreads else {
                guard index < values.count else {
                    self.end()
                    return
                }
                self.watchable.value = values[index]
                self.nextValueAndEnd(values: values, index: index + 1)
                return
            }
            
            self.concurrentQueue.sync {
                guard index < values.count else {
                    self.end()
                    return
                }
                self.watchable.value = values[index]
                self.nextValueAndEnd(values: values, index: index + 1)
                return
            }
        }
    }
    
    public func setValuesAndError(values: [T]) {
        
        nextValueAndError(values: values, index: 0)
    }
    
    private func nextValueAndError(values: [T], index: Int) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(pauseBetweenValues)) {
            
            guard self.useThreads else {
                guard index < values.count else {
                    self.endWithError()
                    return
                }
                self.watchable.value = values[index]
                self.nextValueAndError(values: values, index: index + 1)
                return
            }
            
            self.randomQueue.async {
                guard index < values.count else {
                    self.endWithError()
                    return
                }
                self.watchable.value = values[index]
                self.nextValueAndError(values: values, index: index + 1)
            }
        }
    }
    
    public func setValuesAndDispose(values: [T]) {
        
        nextValueAndDispose(values: values, index: 0)
    }
    
    private func nextValueAndDispose(values: [T], index: Int) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(pauseBetweenValues)) {
            guard self.useThreads else {
                guard index < values.count else {
                    self.endByDisposing()
                    return
                }
                self.watchable.value = values[index]
                self.nextValueAndDispose(values: values, index: index + 1)
                return
            }
        
            self.otherQueue.async {
                guard index < values.count else {
                    self.endByDisposing()
                    return
                }
                self.watchable.value = values[index]
                self.nextValueAndDispose(values: values, index: index + 1)
            }
        }
    }
    
    private func end() {
        
        guard useThreads else {
            end0()
            return
        }
        switch getRandom() {
        case 0:
            end0()
        case 1:
            end1()
        default:
            end2()
        }
    }
    
    private func end0() {
        
        watchable.onComplete()
        endByDisposing()
    }
    
    private func end1() {
        
        someQueue.sync {
            self.watchable.onComplete()
            self.endByDisposing()
        }
    }
    
    private func end2() {
        
        otherQueue.async {
            self.watchable.onComplete()
            self.endByDisposing()
        }
    }
    
    private func endWithError() {
        
        guard useThreads else {
            endWithError0()
            return
        }
        switch getRandom() {
        case 0:
            endWithError0()
        case 1:
            endWithError1()
        default:
            endWithError2()
        }
    }
    
    private func endWithError0() {
        
        let error = NSError(domain: "WatchableTestWrapper.endWithError", code: 0)
        self.watchable.onError(error)
        self.endByDisposing()
    }
    
    private func endWithError1() {
        
        otherQueue.sync {
            let error = NSError(domain: "WatchableTestWrapper.endWithError", code: 1)
            self.watchable.onError(error)
            self.endByDisposing()
        }
    }
    
    private func endWithError2() {
        
        randomQueue.sync {
            let error = NSError(domain: "WatchableTestWrapper.endWithError", code: 2)
            self.watchable.onError(error)
            self.endByDisposing()
        }
    }
    
    private func endByDisposing() {
        
        if disposeBag != nil {
            disposeBag = DisposeBag()
        }
    }
    
    private func getRandom() -> Int {
        
        return Int(arc4random_uniform(3))
    }
}
