# Uatu
Uatu is a (theoretically) powerful yet low-interference way to watch changes in a `Watchable<Element>`.

## How?
### Create and Watch
There are many ways to use Uatu, we'll start with the easiest.
#### Simply watching a local variable
First, create a `Watchable` element:
```swift
let secondsCounter = Watchable(0)
```

which can also be written as:
```swift
let secondsCounter: Watchable<Int>
secondsCounter = Watchable(0)
```

Then create a `Disposable` by `watch`ing the `Watchable` element and providing a closure to handle the `Watchable`'s value changes:
```swift
let secondsWatcher = secondsCounter.watch { seconds in
    print("seconds: \(seconds)")
}
```

which can also be writtten more explicityly as:
```swift
let secondsWatcher = secondsCounter.watch(onNext: { seconds in
    print("seconds: \(seconds)")
})
```

If you run this code now, you will see `seconds: 0` printed to your console because the current value is sent when you being `watch`ing a `Watchable`.  If you do not want the current value, you should use `.watchFuture(` instead of `.watch(`.

If you were only getting a single unchanging value, you wouldn't want nor need to use a library like this, so let's update the secondsCounter value. Update the value of the `Watchable` a few times:
```swift
secondsCounter.value = 1
secondsCounter.value = 1
secondsCounter.value = 2
secondsCounter.value = 3
secondsCounter.value = 5
secondsCounter.value = 8
```

If you run that,  each time the `secondsCounter`’s value is updated (once per second), your `secondsWatcher`’s onNext closure will be called and it will print the new value like so:
```
seconds: 0
seconds: 1
seconds: 1
seconds: 2
seconds: 3
seconds: 5
seconds: 8
```

That's a good way to handle a manually-updated value but not exactly useful. Who manually updates a seconds counter? A (very) slightly more realistic way to update `secondsCounter` would be to update the value once per second using a timer:
```swift
let secondsCounter = Watchable(0)
let timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) {  _ in  self.secondsCounter.value += 1 }
```

and your console will now have the new seconds value once per second, as expected:

```
seconds: 0
seconds: 1
seconds: 2
seconds: 3
etc...
```

If you had a textbox to display the number of seconds, you would do that in your `onNext` closure like this:
```swift
let secondsWatcher = secondsCounter.watch(onNext: { seconds in
    someTextBox.text = seconds
})
```

or:
```swift
let secondsWatcher = secondsCounter.watch(onNext: { seconds in
    someFunctionUpdatesTheTextBox(text: seconds)
})
```

That may be all you need but probably not.

#### Full-powered watching (aka Closer to RL Use)
If you want or need a library like this, you probably won't be watching local `Watchable` variable values. You will probably have classes that encapsulate those `Watchable`s.  Let's refactor our seconds-counting example above, adding a class that encapsulates the `secondsCounter` and exposes methods we'll call on an instance of the class instead of directly interacting with the `Watchable`.
```swift
// BadClock.swift
//

import Foundation
import Uatu

class BadClock {

    private let secondsCounter: Watchable<Int>

    init() {
        self.secondsCounter = Watchable(0)
    }

}
/// TODO: Move this example and others to documentation wiki.
```


### Disposing Disposables
To dispose of the `Disposable`, you can call its `dispose()` method
```swift
secondsWatcher.dispose()
```

and though the timer may continue updating `secondCounter`’s value, you are no longer watching it your closure isn’t called and the values aren’t printed.

You can also create a `DisposeBag` to hold a collection of `Disposeable` objects. If you choose to do this (and you probably should, it makes cleanup much easier to deal with), do not call `dispose()` on the `Disposable`; let the `DisposeBag` take care of that.
Create the `DisposeBag` and add the `Disposable` to the bag when created:
```swift
var disposeBag = DisposeBag()
let clockWatcher = secondsCounter.watch(onNext: { _ in
    print("we're not really doing anything here...")
}).disposed(by: disposeBag)
```

then you dispose of the bag’s items (in a `deinit` or `viewWillDisappear` method, for example) by initialize it again:
```swift
disposeBag = DisposeBag()
```

### Watchable’s onNext, onError, onComplete, onDispose
You can watch and only deal with the changing value (via an `onNext` closure) but you will sometimes need to handle errors, the completion event, and/or the dipose event.  You do this by setting up your `Watchable` in this slightly more verbose manner:
```swift
let secondsCounter = Watchable(0)

let webRequestWatcher = secondsCounter.watch(onNext: { result in 
    print("response from the http call: \(result)")
}, onError: { error in
    print("ERROR: \(error)")
}, onComplete: {
    print("Complete.")
    // perform cleanup or next functionality as needed
}, onDispose: {
    print("Disposed.")
    // perform cleanup or next functionality as needed
}).disposed(by: disposeBag)

// now change the value
secondsCounter.value = 1
// and complete the watch
secondsCounter.onComplete()

```

If your `Watchable` is a single change (like a simple http call, or the even simple example above), you would see this printed to the console:
```
seconds: 0
seconds: 1
Complete.
Disposed.
```

If that http call threw an error instead of a new value, you would see something like:
```
seconds: 0
ERROR: This would be the error description.
Complete.
Disposed.
```
You also simplify the code that changes the value and ends the watch from this:
```swift
// now change the value
secondsCounter.value = 1
// and complete the watch
secondsCounter.onComplete()
```
to this:
```swift
// now change the value and complete the watch
secondsCounter.just(1)
```


If you have an indefinite watcher (like our seconds counter), you may never see a completion event because you will probably continue watching until you clear out the disposeBag but you would be able to handle the disposed event at that time.

If you only want to handle the next and disposed events, you can write it this way:
```swift
let webRequestWatcher = secondsCounter.watch(onNext: { result in 
    print("response from the http call: \(result)")
}, onError: nil,
onComplete: nil,
onDispose: {
    print("Disposed.")
}).disposed(by: disposeBag)
```
which can also be written as:
```swift
let webRequestWatcher = secondsCounter.watch(onNext: { result in 
    print("response from the http call: \(result)")
}, onError: nil,
onComplete: nil) {
    print("Disposed.")
}.disposed(by: disposeBag)
```
which can be simplified to this:
```swift
let webRequestWatcher = secondsCounter.watch(onNext: { result in 
    print("response from the http call: \(result)")
}) {
    print("Disposed.")
}.disposed(by: disposeBag)
```

## TODOs
1. ~~Add a README~~
2. Comments in code [STARTED. INCOMPLETE]
3. Documentation [STARTED. INCOMPLETE]
4. Examples in documentation [STARTED. INCOMPLETE]
5. Complete unit tests to describe, demonstrate and test expected behavior
6. ~~Add array of weak `Disposeable` references to `DisposeBag` and/or `Watchable`~~

## Who?
Shannon J Hager, a software developer based in Charlotte, NC, USA, Earth.

## What?
Uatu is a (theoretically) powerful yet low-interference way to watch changes in a `Watchable<Element>`.

## Where?
[https://bitbucket.org/sirshannon/uatu](https://bitbucket.org/sirshannon/uatu)

## Why?
RxSwift is an incredible Swift port of Rx but is too powerful for most of my use cases.  Taking a cue from [Robert-Hein Hooijmans](https://github.com/roberthein)’s extremely lightweight [Observable](https://github.com/roberthein/Observable) library, Uatu is an attempt to add _just enough_ functionality for my needs and no more.

## When?
Because we love you.
