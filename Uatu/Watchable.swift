//
//  Watchable.swift
//  Uatu
//

import Foundation

/// Item to watch for and react to changes
public final class Watchable<T> {
    
    public typealias OnNextHandler = (T) -> Void
    public typealias OnErrorHandler = (Error) -> Void
    public typealias OnCompleteHandler = () -> Void
    public typealias OnDisposeHandler = () -> Void

    private var watchers: [Int: WeakRef<Watcher>] = [:]
    
    private var uniqueID = (0...).makeIterator()
    
    private let concurrentQueue = DispatchQueue(label: "concurrent queue",
                                                qos: .userInitiated,
                                                attributes: [DispatchQueue.Attributes.concurrent])
    private let semaphore = DispatchSemaphore(value: 1)
    
    public var value: T {
        
        didSet {
            watchers.forEach { $0.value.value?.onNext(self.value) }
        }
    }
    
    public init(_ value: T) {
        
        self.value = value
    }
    
    public func onNext(_ value: T) {
        
        self.value = value
    }
    
    public func onError(_ error: Error) {
        
        watchers.forEach { $0.value.value?.onError(error) }
    }
    
    public func onComplete() {
        
        watchers.forEach { $0.value.value?.onComplete() }
    }
    
    public func just(_ value: T) {
        
        self.value = value
        onComplete()
    }
    
    public func watch(_ onNext: @escaping OnNextHandler) -> Disposable {
        
        return watch(onNext: onNext)
    }
    
    public func watch(onNext: @escaping OnNextHandler,
                      onError: OnErrorHandler? = nil,
                      onComplete: OnCompleteHandler? = nil,
                      onDispose: OnDisposeHandler? = nil) -> Disposable {
        
        return addWatcher(onNext: onNext,
                          onError: onError,
                          onComplete: onComplete,
                          onDispose: onDispose,
                          onlyFuture: false)
    }
    
    public func watchFuture(_ onNext: @escaping OnNextHandler) -> Disposable {
        
        return watchFuture(onNext: onNext)
    }
    
    public func watchFuture(onNext: @escaping OnNextHandler,
                            onError: OnErrorHandler? = nil,
                            onComplete: OnCompleteHandler? = nil,
                            onDispose: OnDisposeHandler? = nil) -> Disposable {
        
        return addWatcher(onNext: onNext,
                          onError: onError,
                          onComplete: onComplete,
                          onDispose: onDispose,
                          onlyFuture: true)
    }
    
    private func addWatcher(onNext: @escaping OnNextHandler,
                            onError: OnErrorHandler?,
                            onComplete: OnCompleteHandler?,
                            onDispose: OnDisposeHandler? = nil,
                            onlyFuture: Bool? = false) -> Disposable {
        
        var disposable: Disposable!
        
        concurrentQueue.sync {
            
            self.semaphore.wait()
            guard let id = uniqueID.next() else {
                fatalError("Failed to get new uniqueId.")
            }
            
            let watcher = Watcher(id: id)
            watcher.addOnNext(onNext)
            watcher.addOnError(onError)
            watcher.addOnComplete(onComplete)
            watcher.addOnDispose(onDispose)
            
            disposable = watcher.getDisposable()
            
            let weakWatcher = WeakRef<Watcher>(watcher)
            self.watchers[id] = weakWatcher
            
            if onlyFuture != true {
                watcher.onNext(self.value)
            }
            self.semaphore.signal()
        }
        return disposable
    }

    private class Watcher {
        
        private let id: Int
        private var onNextHandler: OnNextHandler? = nil
        private var onErrorHandler: OnErrorHandler? = nil
        private var onCompleteHandler: OnCompleteHandler? = nil
        private var onDisposeHandler: OnDisposeHandler? = nil
        private var weakDisposable: WeakRef<AnyObject>? = nil
        private var isDiposed = false
        
        init(id: Int) {
            
            self.id = id
        }
        
        fileprivate func addOnNext(_ onNextHandler: OnNextHandler?) {
            
            self.onNextHandler = onNextHandler
        }
        
        func onNext(_ value: T) {
            
            onNextHandler?(value)
        }
        
        fileprivate func addOnError(_ onErrorHandler: OnErrorHandler?) {
            
            self.onErrorHandler = onErrorHandler
        }
        
        fileprivate func onError(_ error: Error) {
            
            onNextHandler = nil
            onErrorHandler?(error)
            onErrorHandler = nil
            onCompleteHandler?()
            onCompleteHandler = nil
            onDisposeHandler?()
            onDisposeHandler = nil
            guard let wd = weakDisposable?.value as? Disposable?,
                let disposable = wd else {
                return
            }
            disposable.dispose()

        }
        
        fileprivate func addOnComplete(_ onCompleteHandler: OnCompleteHandler?) {
            
            self.onCompleteHandler = onCompleteHandler
        }
        
        fileprivate func onComplete() {
            
            onNextHandler = nil
            onErrorHandler = nil
            onCompleteHandler?()
            onCompleteHandler = nil
            onDisposeHandler?()
            onDisposeHandler = nil
            guard let wd = weakDisposable?.value as? Disposable?,
                let disposable = wd else {
                    return
            }
            disposable.dispose()
        }
        
        fileprivate func addOnDispose(_ onDisposeHandler: OnDisposeHandler?) {
            
            self.onDisposeHandler = onDisposeHandler
        }
        
        fileprivate func onDispose() {
            
            onNextHandler = nil
            onErrorHandler = nil
            onCompleteHandler = nil
            onDisposeHandler?()
            onDisposeHandler = nil
            weakDisposable = nil
            isDiposed = true
        }
        
        fileprivate func getDisposable() -> Disposable? {
            guard isDiposed == false else {
                return nil
            }
            
            guard let wd = weakDisposable?.value as? Disposable?,
                let disposable = wd else {
                    let _disposable = Disposable(id: id) {
                            self.onDispose()
                        }
                    weakDisposable = WeakRef<AnyObject>(_disposable)
                    return _disposable
            }
            return disposable
        }
    }
}
