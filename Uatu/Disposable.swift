//
//  Disposable.swift
//  Uatu
//

import Foundation

/**
 Class to represent a disposable resource when watching for changes to a `Watchable`'s value
 */
public final class Disposable {
    
    /// id used to reference the watcher's closures in `Watchable`'s event handlers
    private let id: Int
    
    /// closure to be called on `Watchable` when this instance is disposed.
    private let _dispose: () -> ()
    
    /// `DisposeBag` that will dispose this instance
    weak private var disposeBag: DisposeBag?
    
    /// has this instance been disposed?
    private var isDisposed = false
    
    /**
     Initialize with the watcher's id and onDispose closure
     
     - parameter id: int Id used by `Watchable` to reference a specific watcher's
     event handler closures
     - parameter onDispose: closure to be called when this item is disposed
     */
    public init(id: Int, _ onDispose: @escaping () -> ()) {
        self.id = id
        self._dispose = onDispose
    }
    
    deinit {
        // call dispose when this instance is deinitialized
        dispose()
    }
    
    /**
     Diposes of this instance
     */
    public func dispose() {
        // remove the disposable from its disposeBag (if exists)
        disposeBag?.remove(self)
        // clear reference to disposeBag (if exists)
        disposeBag = nil
        // exit early if already ran the dipose method
        guard isDisposed == false else { return }
        // set isDisposed = true so we only run this one
        isDisposed = true
        // run the onDispose closure used to initialize this disposable
        _dispose()
    }
    
    /**
     Specifies the `DisposeBag` that will be used to dispose of this instance
     
     - parameter disposeBag: the `DisposeBag` that will dispose of this instance
     */
    public func disposed(by disposeBag: DisposeBag) {
        disposeBag.add(self)
        self.disposeBag = disposeBag
    }
}

/// Allows us to compare two `Disposable`s
extension Disposable: Equatable {
    
    public static func == (lhs: Disposable, rhs: Disposable) -> Bool {
        return lhs.id == rhs.id
    }
}
