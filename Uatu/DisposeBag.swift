//
//  DisposeBag.swift
//  Uatu
//

import Foundation

/**
 Class to hold array of `Disposable`s to ease in disposing of those items at the
 same time with one call.
 
 Use this class to collection a related group of `Disposable`s and dispose of
 them at the same time, such as when a viewController will disappear or other times
 when the values no longer need to be watched.
 */
public final class DisposeBag {
    
    /// array of disposables items
    private var items: [Disposable] = []
    
    /// number of items in this `DisposeBag` instance. Probably only used for debugging.
    public var count: Int {
     
        return concurrentQueue.sync {
            return items.count
        }
    }
    
    private let concurrentQueue = DispatchQueue(label: "concurrent queue", qos: .userInitiated, attributes: [DispatchQueue.Attributes.concurrent])
    
    public init() {}
    
    /**
     Adds a `Disposable` item to the internal collection of items
     
     - parameter item: optional `Disposable` item to be added to the private
     array of items
     */
    public func add(_ item: Disposable?) {
        
        guard let item = item else { return }
        
        concurrentQueue.async(flags: .barrier) {
            self.items.append(item)
        }
    }
    
    /**
     Removes a specified `Disposable` from the private array of items, if it exists
     
     - parameter item: `Disposable` item to be removed from the private array of items
     */
    internal func remove(_ item: Disposable) {
        
        concurrentQueue.async(flags: .barrier) {
            if let index = self.items.index(where: { disposable in
                return disposable === item
            }) {
                self.items.remove(at: index)
            }
        }
    }
    
    deinit {
        
        // call dispose on all `Disposable`s in the private array of items when
        // this instance is deinitialized
        items.forEach { $0.dispose() }
    }
}
